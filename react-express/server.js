const express = require("express");
const app = express();
const port = 5000;
app.get("/api/customers", (req, res) => {
  const customers = [
    { id: 1, firstName: "John", lastName: "Doe" },
    { id: 2, firstName: "Steve", lastName: "Smith" },
    { id: 3, firstName: "Mary", lastName: "Swanson" },
    { id: 4, firstName: "Ali", lastName: "Hasan" }
  ];
  res.json(customers);
});
app.listen(port, () => console.log(`server is running on port ${port}`));
